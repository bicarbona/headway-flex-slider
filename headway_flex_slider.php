<?php
/**
 * Plugin Name: Headway Flex Slider
 * Plugin URI:  https://bitbucket.org/bicarbona/headway-flex-slider
 * Description: Headway Flex Slider
 * Version:     0.1.6.3
 * Author:      Bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: headway_flex_slider
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bitbucket.org/bicarbona/headway-flex-slider
 * Bitbucket Branch: master
 */

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'headway_flex_slider_register');
function headway_flex_slider_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('headway_flex_sliderBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'headway_flex_slider_prevent_404');
function headway_flex_slider_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'headway_flex_slider_redirect');
function headway_flex_slider_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}