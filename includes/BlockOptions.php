<?php

    class headway_flex_sliderBlockOptions extends HeadwayBlockOptionsAPI {
	
	public $tabs = array(
		'setup' => 'Slider Setup',
		'query-filters' => 'Query Filters',
		'special' => 'Special'
	);

/**

Options

**/

 function modify_arguments($args) {

  	$block = $args['block'];

//	$this->tab_notices['my-settings'] = self::picus(). HeadwayBlocksData::get_block_name($args['block_id']) . HeadwayBlockAPI::get_setting($block, 'post-type');

	$post_type = HeadwayBlockAPI::get_setting($block, 'post-type');

		$this->inputs['query-filters']['post-id'] = array(
			'type' => 'multi-select',
			'name' => 'post-id',
			'label' => 'POST',
			'options' => self::hw_get_post($block)
		);
	 }

public $inputs = array(
	'setup' => array(		
/**
Image / Excerpt / Navigation
**/
		'heading-item-excerpt-content'	=> array(
			'name'	 => 'heading-item-excerpt-content',
			'type'	 => 'heading',
			'label'	 => ' Image / Excerpt - Content'
		),

		// 'show-images' => array(
		// 	'type' => 'checkbox',
		// 	'name' => 'show-images', 
		// 	'label' => 'Show Images',
		// 	'default' => true,
		// ),

		'slider_type'	=> array(
			'name'	 => 'slider_type',
			'type'	 => 'select',
			'label'	 => 'Slider Layout Type',
			'default' => 'single_featured',
			'options' => array(
				'single_featured' => 'single_featured',
				'single_excerpt' => 'single_excerpt',
		 		'columns_excerpt_featured' => 'columns_excerpt_featured',
				'columns_featured_excerpt' => 'columns_featured_excerpt',
			),
			// 'tooltip' => '// columns_excerpt_featured / columns_featured_excerpt / single_featured / single_excerpt'
		),

		'show-item-excerpt-content'	=> array(
			'name'	 => 'show-item-excerpt-content',
			'type'	 => 'select',
			'label'	 => 'Show Excerpt',
			// 'tooltip' => 'tooltip',
			'options' => array(
				'hide-option' => 'Hide',
				'show-option' => 'Show'
			),

			'toggle' => array(
				'hide-option' => array(
					'hide' => array(
						'#input-show-item-excerpt',
						'#input-item-excerpt-content'
					),
				),
				'show-option' => array(
					'show' => array(
						'#input-show-item-excerpt',
						'#input-item-excerpt-content'
					),
				),
			),
			'callback' => 'id = $(input).attr("block_id");'
		),

		'item-excerpt-content'	=> array(
			'name'	 => 'item-excerpt-content',
			'type'	 => 'select',
			'label'	 => 'Customize mode',
			'tooltip' => 'tooltip',
			'options' => array(
				'excerpt-option' => 'Excerpt',
				'my-excerpt-option' => 'My Excerpt',
				'content-option' => 'Content'
			),
			'toggle' => array(
				'excerpt-option' => array(
					'hide' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),
				'my-excerpt-option' => array(
					'show' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),
				'content-option' => array(
					'hide' => array(
						'#input-my-excerpt-limit',
						'#input-length-type',
						'#input-finish',
						'#input-no-custom',
						'#input-add-link',
						'#input-read-more',
						'#input-ellipsis',
						'#input-no-shortcode',
						'#input-heading-advanced-excerpt',
						'#input-exclude-tags',
						'#input-allowed-tags',
					),
				),

			),
			'callback' => 'id = $(input).attr("block_id");'
		),
			'show-navigation'	=> array(
				'name'	 => 'show-navigation',
				'type'	 => 'select',
				'label'	 => 'Navigation',
			//	'tooltip' => 'tooltip',
				'options' => array(
					'hide-options' => 'Hide',
					'show-options' => 'Show'
				),
				'toggle' => array(
					'hide-options' => array(
						'hide' => array(
							'#input-prev-nav',
							'#input-next-nav'
						),
					),
					'show-options' => array(
						'show' => array(
							'#input-prev-nav',
							'#input-next-nav'
						),
					),
				),
				'callback' => 'id = $(input).attr("block_id");'
			),

			'prev-nav'	=> array(
				'name'	 => 'prev-nav',
				'type'	 => 'select',
				'label'	 => 'Navigation Prev',
				'default' => 'ss-navigateleft',
				'options' => array(
					'ss-navigateleft' => 'ss-navigateleft',
					'fa fa-caret-left' => 'fa fa-caret-left',
			 		//'right' => 'right'
				),
			//	'tooltip' => 'tooltip'
			),

			'next-nav'	=> array(
				'name'	 => 'next-nav',
				'type'	 => 'select',
				'label'	 => 'Navigation Next',
				'default' => 'ss-navigateright',
				'options' => array(
					'ss-navigateright' => 'ss-navigateright',
					'fa fa-caret-right' => 'fa fa-caret-right',
				),
			//	'tooltip' => 'tooltip'
			),
		/**
		Advanced Except
		**/
			'heading-advanced-excerpt'	=> array(
				'name'	 => 'heading-advanced-excerpt',
				'type'	 => 'heading',
				'label'	 => 'Advanced Excerpt'
			),

			'length-type'	=> array(
				'name'	 => 'length-type',
				'type'	 => 'select',
				'label'	 => 'Length Type',
				'default' => 'left',
				'options' => array(
					'words' => 'words',
			 		'characters' => 'characters'
				),
				// 'tooltip' => 'tooltip'
			),

			'my-excerpt-limit'	=> array(
				'name'	 => 'my-excerpt-limit',
				'type'	 => 'slider',
				'slider-min' => 10,
				'slider-max' => 150,
				'slider-interval' =>1,
				'label'	 => 'My Excerpt Limit',
				'default' => 10,
				'tooltip' => 'tooltip'
			),

			'no-custom'	=> array(
				'name'	 => 'no-custom',
				'type'	 => 'checkbox',
				'label'	 => 'no-custom',
				'default' => false,
				// 'tooltip' => 'tooltip'
			),

			'add-link'	=> array(
				'name'	 => 'add-link',
				'type'	 => 'checkbox',
				'label'	 => 'add-link',
				'default' => false,
				// 'tooltip' => 'tooltip'
			),

			'read-more'	=> array(
				'name'	 => 'read-more',
				'type'	 => 'text',
				'label'	 => 'Read more text',
				'tooltip' => 'enumeration, if set to `exact` the excerpt will be the exact lenth as defined by the "Excerpt Length" option. If set to `word` the last word in the excerpt will be completed. If set to `sentence` the last sentence in the excerpt will be completed'
			),

			'finish'	=> array(
				'name'	 => 'finish',
				'type'	 => 'select',
				'label'	 => 'finish',
				'default' => 'left',
				'options' => array(
					// exact, word, sentence
					'exact' => 'exact',
			 		'word' => 'word',
			 		'sentence' => 'sentence'
				),
				// 'tooltip' => 'tooltip'
			),

			'no-shortcode'	=> array(
				'name'	 => 'no-shortcode',
				'type'	 => 'checkbox',
				'label'	 => 'No Shortcode',
				'default' => false,
				'tooltip' => 'tooltip'
			),

			'ellipsis'	=> array(
				'name'	 => 'ellipsis',
				'type'	 => 'text',
				'label'	 => 'ellipsis',
				'tooltip' => '&amp;hellip;'
			),
			'exclude-tags'	=> array(
				'name'	 => 'exclude-tags',
				'type'	 => 'text',
				'label'	 => 'Exclude Tags',
				'tooltip' => ' img,p,strong a comma-separated list of HTML tags that must be removed from the excerpt. Using this setting in combination with `allowed_tags` makes no sense'
			),

			'allowed-tags'	=> array(
				'name'	 => 'allowed-tags',
				'type'	 => 'text',
				'label'	 => 'Allowed Tags',
				'tooltip' => ' img,p,strong a comma-separated list of HTML tags that must be removed from the excerpt. Using this setting in combination with `allowed_tags` makes no sense'
			),

		'heading-item-title'	=> array(
			'name'	 => 'heading-item-title',
			'type'	 => 'heading',
			'label'	 => 'Item Title'
		),

		'show-item-title'	=> array(
			'name'	 => 'show-item-title',
			'type'	 => 'checkbox',
			'label'	 => 'Show Item Title',
			'default' => false,
			// 'tooltip' => 'tooltip'
		),

		'item-title-html-tag' => array(
			'type' => 'select',
			'name' => 'item-title-html-tag',
			'label' => 'Title HTML tag',
			'default' => 'h1',
			'options' => array(
				'h1' => '&lt;H1&gt;',
				'h2' => '&lt;H2&gt;',
				'h3' => '&lt;H3&gt;',
				'h4' => '&lt;H4&gt;',
				'h5' => '&lt;H5&gt;',
				'h6' => '&lt;H6&gt;',
				'span' => '&lt;span&gt;'
			)
		),

		'item-title-link' => array(
			'type' => 'checkbox',
			'name' => 'item-title-link',
			'label' => 'Link Title?'
		),

		'show-item-title-before'	=> array(
			'name'	 => 'show-item-title-before',
			'type'	 => 'checkbox',
			'label'	 => 'Title before content',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),
		
		'show-edit-link'	=> array(
			'name'	 => 'show-edit-link',
			'type'	 => 'checkbox',
			'label'	 => 'Show Edit',
			'default' => false,
			// 'tooltip' => 'tooltip'
		),
		
	/**
	Animation
	**/
		'animation-heading'	=> array(
			'name'	 => 'animation-heading',
			'type'	 => 'heading',
			'label'	 => 'Animation'
		),

/**
Flex Slider Properties
https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
**/
		'animation'	=> array(
			'name'	 => 'animation',
			'type'	 => 'select',
			'label'	 => 'Animation',
			'default' => 'fade',
			'options' => array(
				'fade' => 'fade',
				'slide' => 'slide',
			),
		//'tooltip' => 'tooltip'
		),

		'easing'	=> array(
			'name'	 => 'easing',
			'type'	 => 'select',
			'label'	 => 'Easing animation',
			'default' => 'swing',
			'options' => array(
				"linear" => "linear" ,
				"swing" => "swing",
				"easeInQuad" => "easeInQuad" ,
				"easeOutQuad" => "easeOutQuad",
				"easeInOutQuad" => "easeInOutQuad",
				"easeInCubic" => "easeInCubic" ,
				"easeOutCubic" => "easeOutCubic" ,
				"easeInOutCubic" => "easeInOutCubic" ,
				"easeInQuart" => "easeInQuart" ,
				"easeOutQuart" => "easeOutQuart" ,
				"easeInOutQuart" => "easeInOutQuart",
				"easeInQuint" => "easeInQuint",
				"easeOutQuint" => "easeOutQuint",
				"easeInOutQuint" => "easeInOutQuint",
				"easeInExpo" => "easeInExpo" ,
				"easeOutExpo" => "easeOutExpo",
				"easeInOutExpo" => "easeInOutExpo" ,
				"easeInSine" => "easeInSine" ,
				"easeOutSine" => "easeOutSine" ,
				"easeInOutSine" => "easeInOutSine",
				"easeInCirc" => "easeInCirc" ,
				"easeOutCirc" => "easeOutCirc" ,
				"easeInOutCirc" => "easeInOutCirc" ,
				"easeInElastic" => "easeInElastic" ,
				"easeOutElastic" => "easeOutElastic" ,
				"easeInOutElastic" => "easeInOutElastic",
				"easeInBack" => "easeInBack" ,
				"easeOutBack" => "easeOutBack" ,
				"easeInOutBack" => "easeInOutBack",
				"easeInBounce" => "easeInBounce" ,
				"easeOutBounce" => "easeOutBounce" ,
				"easeInOutBounce" => "easeInOutBounce" 
			),
			//'tooltip' => 'tooltip'
		),

		'smoothheight'	=> array(
			'name'	 => 'smoothheight',
			'type'	 => 'checkbox',
			'label'	 => 'Smooth Height',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'direction'	=> array(
			'name'	 => 'direction',
			'type'	 => 'select',
			'label'	 => 'Direction',
			'default' => 'horizontal',
			'options' => array(
				'horizontal' => 'horizontal',
		 		'vertical' => 'vertical'
			),
			//'tooltip' => 'tooltip'
		),

		'slideshow'	=> array(
			'name'	 => 'slideshow',
			'type'	 => 'checkbox',
			'label'	 => 'Slideshow Auto start',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),
		'pauseonhover'	=> array(
			'name'	 => 'pauseonhover',
			'type'	 => 'checkbox',
			'label'	 => 'Pause On Hover',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'touch'	=> array(
			'name'	 => 'touch',
			'type'	 => 'checkbox',
			'label'	 => 'Touch',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'animationloop'	=> array(
			'name'	 => 'animationloop',
			'type'	 => 'checkbox',
			'label'	 => 'Animation Loop',
			'default' => true,
			// 'tooltip' => 'tooltip'
		),

		'mousewheel'	=> array(
			'name'	 => 'mousewheel',
			'type'	 => 'checkbox',
			'label'	 => 'Mouse Wheel',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),

		'keyboard'	=> array(
			'name'	 => 'keyboard',
			'type'	 => 'checkbox',
			'label'	 => 'Keyboard',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'multiplekeyboard'	=> array(
			'name'	 => 'multiplekeyboard',
			'type'	 => 'checkbox',
			'label'	 => 'Multiple Keyboard',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),

		'controlnav'	=> array(
			'name'	 => 'controlnav',
			'type'	 => 'checkbox',
			'label'	 => 'Control Nav',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),

		'directionnav'	=> array(
			'name'	 => 'directionnav',
			'type'	 => 'checkbox',
			'label'	 => 'Direction Nav',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),

		'slideshow'	=> array(
			'name'	 => 'slideshow',
			'type'	 => 'checkbox',
			'label'	 => 'Slideshow',
			'default' => true,
		//	'tooltip' => 'tooltip'
		),

		'slideshowspeed'	=> array(
			'name'	 => 'slideshowspeed',
			'type'	 => 'slider',
			'slider-min' => 600,
			'slider-max' => 40000,
			'slider-interval' =>100,
			'label'	 => 'Slideshow speed',
			'default' => 5000,
			//'tooltip' => 'tooltip'
		),

		'animationspeed' => array(
			'name'	 => 'animationspeed',
			'type'	 => 'slider',
			'slider-min' => 100,
			'slider-max' => 2000,
			'slider-interval' => 50,
			'label'	 => 'Animation Speed',
			'default' => 300,
			//'tooltip' => 'tooltip'
		),

		'startat'	=> array(
			'name'	 => 'startat',
			'type'	 => 'slider',
			'slider-min' => 0,
			'slider-max' => 30,
			'slider-interval' =>1,
			'label'	 => 'Start At',
			'default' => 0,
			// 'tooltip' => 'tooltip'
		),

		'randomize'	=> array(
			'name'	 => 'randomize',
			'type'	 => 'checkbox',
			'label'	 => 'Randomize',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),
		
		'usecss' => array(
			'name'	 => 'usecss',
			'type'	 => 'checkbox',
			'label'	 => 'Use CSS',
			'default' => true,
			//'tooltip' => 'tooltip'
		),

		'initdelay' => array(
			'name'	 => 'initdelay',
			'type'	 => 'slider',
			'slider-min' => 0,
			'slider-max' => 8000,
			'slider-interval' => 50,
			'label'	 => 'Init Delay',
			'default' => 0,
			//'tooltip' => 'tooltip'
		),
		
		'video'	=> array(
			'name'	 => 'video',
			'type'	 => 'checkbox',
			'label'	 => 'Video',
			'default' => false,
		//	'tooltip' => 'tooltip'
		),

		// Title
		'heading-title'	=> array(
			'name'	 => 'heading-title',
			'type'	 => 'heading',
			'label'	 => 'Title'
		),
			'title'	=> array(
				'name'	 => 'title',
				'type'	 => 'text',
				'label'	 => 'Title',
				//'tooltip' => 'tooltip'
			),

			'title-html-tag' => array(
				'type' => 'select',
				'name' => 'title-html-tag',
				'label' => 'Title HTML tag',
				'default' => 'h1',
				'options' => array(
					'h1' => '&lt;H1&gt;',
					'h2' => '&lt;H2&gt;',
					'h3' => '&lt;H3&gt;',
					'h4' => '&lt;H4&gt;',
					'h5' => '&lt;H5&gt;',
					'h6' => '&lt;H6&gt;',
					'span' => '&lt;span&gt;'
				)
			),

			'item-title-link' => array(
				'type' => 'checkbox',
				'name' => 'item-title-link',
				'label' => 'Link Title?'
			),
			
		// Debug
		'debug-heading'	=> array(
			'name'	 => 'debug-heading',
			'type'	 => 'heading',
			'label'	 => 'Debug Info'
		),
			
			'debug' => array(
				'type' => 'checkbox',
				'name' => 'debug', 
				'label' => 'Show: DEBUG',
				'default' => false,
				'tooltip' => 'Show a . ',
			),
		),

/**

Query

**/
		'query-filters' => array(
			
			// 'paginate' => array(
			// 	'type' => 'checkbox',
			// 	'name' => 'paginate',
			// 	'label' => 'Paginate Featuress',
			// 	'tooltip' => '',
			// 	'default' => true,
			// 	'tooltip' => 'Enabling pagination adds buttons to the bottom of the pin board to go to the next/previous page.  <strong>Note:</strong> If infinite scrolling is enabled, pagination will be hidden. '
			// ),

			'categories' => array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Categories',
				'tooltip' => '',
				'options' => 'get_categories()',
				'tooltip' => 'Filter the pins that are shown by categories. '
			),
			
			'categories-mode' => array(
				'type' => 'select',
				'name' => 'categories-mode',
				'label' => 'Categories Mode',
				'tooltip' => '',
				'options' => array(
					'include' => 'Include',
					'exclude' => 'Exclude'
				),
				'tooltip' => 'If this is set to <em>include</em>, then only the pins that match the categories filter will be shown.  If set to <em>exclude</em>, all pins that match the selected categories will not be shown. '
			),
			
			'post-type' => array(
				'type' => 'select',
				'name' => 'post-type',
				'label' => 'Post Type',
				'tooltip' => '',
				'options' => 'get_post_types()'
			),

			
			'order' => array(
				'type' => 'select',
				'name' => 'order',
				'label' => 'Order',
				'tooltip' => '',
				'options' => array(
					'desc' => 'Descending',
					'asc' => 'Ascending',
				)
			)
		),


		'special' => array(

	       'typer-js'	=> array(
	       	'name'	 => 'typer-js',
	       	'type'	 => 'checkbox',
	       	'label'	 => 'Typer.js',
	       	'default' => false,
	       	// 'tooltip' => 'tooltip'
	       ),


	       'min-height'	=> array(
	       	'name'	 => 'min-height',
	       	'type'	 => 'slider',
	       	'slider-min' => 1,
	       	'slider-max' => 1000,
	       	'slider-interval' => 1,
	       	'label'	 => 'Min Height',
	       	'default' => 80,
	       	// 'tooltip' => 'tooltip'
	       ),
		                   
		),

	);

	/**
	 * Get Categories
	 * @return Array [description]
	 */
	function get_categories() {
		
		$category_options = array();
		
		$categories_select_query = get_categories();
		
		foreach ($categories_select_query as $category)
			$category_options[$category->term_id] = $category->name;

		return $category_options;
		
	}
	
	function get_authors() {
		
		$author_options = array();
		
		$authors = get_users(array(
			'orderby' => 'post_count',
			'order' => 'desc',
			'who' => 'authors'
		));
		
		foreach ( $authors as $author )
			$author_options[$author->ID] = $author->display_name;
			
		return $author_options;
		
	}
	
	
	function get_post_types() {
		
		$post_type_options = array();

		$post_types = get_post_types(false, 'objects'); 
			
		foreach($post_types as $post_type_id => $post_type){
			
			//Make sure the post type is not an excluded post type. 
			if(in_array($post_type_id, array('revision', 'nav_menu_item'))) 
				continue;
			
			$post_type_options[$post_type_id] = $post_type->labels->name;
		
		}
		
		return $post_type_options;
		
	}

	function hw_get_post($block) {
	
		$category_options = array();

		$args = array(
		    'numberposts'    => -1,
		    'orderby'      => 'post_date',
		    'order'       => 'DESC',
		    	'post_type'     => HeadwayBlockAPI::get_setting($block, 'post-type'), // Dolezite takto sa predava parameter v ramci options
		    'suppress_filters' => true
		  );

			$posts = get_posts($args);
		 
		 foreach ( $posts as $post ) {
			$category_options[ $post->ID ] = get_the_title( $post->ID );
		  }
	
		return ($category_options);
	}
	
}