!function ($) {
$(window).load(function(){
$('.flexslider').flexslider({
		selector: '.slides > .item',
		slideshow: flex_vars.slideshow,    //Boolean: Animate slider automatically
		smoothHeight: flex_vars.smoothHeight,
		controlNav: flex_vars.controlNav,
		directionNav: flex_vars.directionNav,
		pauseOnHover: flex_vars.pauseonHover,
		initDelay: flex_vars.initDelay,
		startAt: flex_vars.startAt,
		useCSS: flex_vars.useCSS,
		animation: flex_vars.animation,
		direction: flex_vars.direction,
		easing: flex_vars.easing,
		slideshowSpeed: flex_vars.slideshowSpeed,
		animationSpeed: flex_vars.animationSpeed,
		keyboard: flex_vars.keyboard,
		multipleKeyboard: flex_vars.multipleKeyboard,
		touch: flex_vars.touch,
		mousewheel: flex_vars.mousewheel,
		video: flex_vars.video,
		animationLoop: flex_vars.animationloop,
		// minItems: 2,
		// maxItems: 2,
		// move:2,
		start: function(slider) {
		  	slider.removeClass('loading');
		}
	  //    animation: jQuery(this).data('transition'),
	});
});

}(window.jQuery);;
// javascript file

// console.log( "animationLoop", flex_vars.animationLoop);
// console.log( "direction", flex_vars.direction);
// console.log( "randomize", flex_vars.randomize);
// console.log( "smoothHeight", flex_vars.smoothHeight);
// console.log( "startAt", flex_vars.startAt);
// console.log( "useCSS", flex_vars.useCSS);
// console.log( "video", flex_vars.video);
// console.log( "slideshow", flex_vars.slideshow);
// console.log( "initDelay", flex_vars.initDelay);
// console.log( "animationSpeed", flex_vars.animationSpeed);
// console.log( "slideshowSpeed", flex_vars.slideshowSpeed);
// console.log( "touch", flex_vars.touch);
// console.log( "easing", flex_vars.easing);
// console.log( "directionNav", flex_vars.directionNav);
// console.log( "keyboard", flex_vars.keyboard);
// console.log( "mousewheel", flex_vars.mousewheel);
// console.log( "multipleKeyboard", flex_vars.multipleKeyboard);
// console.log( "animation", flex_vars.animation);
// console.log( "controlNav", flex_vars.controlNav);
// console.log( "pauseOnHover", flex_vars.pauseOnHover);