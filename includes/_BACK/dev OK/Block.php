<?php
class headway_flex_sliderBlock extends HeadwayBlockAPI {

    public $id = 'headway_flex_slider';
    public $name = 'Flex Slider';
    public $options_class = 'headway_flex_sliderBlockOptions';
    public $description = 'Flex Slider';

	function init() {

	}

// Link na archiv post type
 public function get_archive_link( $post_type ) {
    global $wp_post_types;
    $archive_link = false;
    if (isset($wp_post_types[$post_type])) {
      $wp_post_type = $wp_post_types[$post_type];
      if ($wp_post_type->publicly_queryable)
        if ($wp_post_type->has_archive && $wp_post_type->has_archive!==true)
          $slug = $wp_post_type->has_archive;
        else if (isset($wp_post_type->rewrite['slug']))
          $slug = $wp_post_type->rewrite['slug'];
        else
          $slug = $post_type;
      $archive_link = get_option( 'siteurl' ) . "/{$slug}/";
    }
    return apply_filters( 'archive_link', $archive_link, $post_type );
  }

	function enqueue_action($block) {

		if ( !$block )
		$block = HeadwayBlocksData::get_block($block_id);

		/* CSS */
		wp_enqueue_style('headway-flexslider-posts', plugin_dir_url(__FILE__) . '/css/flexslider.css');
		/* JS */

		wp_enqueue_script('headway-flexslider', plugin_dir_url(__FILE__) . 'js/jquery.flexslider-min.js', array('jquery'));
		
		wp_enqueue_script('flexslider', plugin_dir_url(__FILE__) . 'js/flexslider.js', array('jquery'));


	/* HW option */

	/** TEMP TODO dorobit **/
		$shown_min = parent::get_setting($block, 'shown-min', 1);		
		$shown_max = parent::get_setting($block, 'shown-max', 3);

		$show_navigation = parent::get_setting($block, 'show-navigation');
		$prev_nav = parent::get_setting($block, 'prev-nav', 'ss-navigateleft');
		$next_nav = parent::get_setting($block, 'next-nav', 'ss-navigateright');
	/** TEMP **/
	
		$slideshow = parent::get_setting($block,'slideshow', true);
		$animation = parent::get_setting($block,'animation', 'fade');
		$easing = parent::get_setting($block,'easing', 'swing');
		$animationspeed = parent::get_setting($block,'animationspeed', '600');
		$smoothheight = parent::get_setting($block,'smoothheight', true);
		$randomize = parent::get_setting($block,'randomize', true);
		$pauseonhover = parent::get_setting($block,'pauseonhover', false);
		$touch = parent::get_setting($block,'touch', true);
		$keyboard = parent::get_setting($block,'keyboard', true);
		$multiplekeyboard = parent::get_setting($block,'multiplekeyboard', false);
		$controlnav = parent::get_setting($block,'controlnav', false);
		$slideshowspeed = parent::get_setting($block,'slideshowspeed', '5000');
		$directionnav = parent::get_setting($block,'directionnav', false);
		$usecss = parent::get_setting($block,'usecss', true);
		$initdelay = parent::get_setting($block,'initdelay', 0);
		$startat = parent::get_setting($block,'startat', 0);
		$video = parent::get_setting($block,'video', false);
		$direction = parent::get_setting($block,'direction', 'horizontal');
		$mousewheel = parent::get_setting($block,'mousewheel', false);
		$animationloop = parent::get_setting($block,'animationloop', true);

		$data = array(
			  //'flex_auto' => ($options['slide-auto']) ? 'true' : 'false',
			'slideshow' => $slideshow,
			'startAt' => $startat,
			'randomize' => $randomize,
			'slideshow'	=> $slideshow,
			'slider'	=> $slider,
			'pauseOnHover' => $pauseonhover,
			'smoothHeight' => $smoothheight,
			'animation' => $animation,
			'selector' => '.slides > .row',
			'easing' => $easing,
			'slideshowSpeed' => $slideshowspeed,
			'animationSpeed' => $animationspeed,
			'directionNav' => $directionnav,
			'keyboard' => $keyboard,
			'multipleKeyboard'=> $multiplekeyboard,
			'touch' => $touch,
			'controlNav' => $controlnav,
			'initDelay' => $initdelay,
			'useCSS' => $usecss,
			'video' => $video,
			'startAt' => $startat,
			'direction' => $direction,
			'mousewheel' => $mousewheel,
			'animationLoop' => $animationloop
			);

		wp_localize_script('flexslider', 'flex_vars', $data	);
	}


	// function dynamic_css($block_id) {

	// //	return '#block-' . $block_id . ' .featured-posts-featured { margin-bottom: ' . parent::get_setting($block_id, 'featured-bottom-margin', 15) . 'px; }';

	// }

/**

Excerpt / Content

**/
function excerpt_content($block, $excerpt_content_class){

/* Content */

		// $show_titles = parent::get_setting($block, 'show-titles', true);

		$show_continue = parent::get_setting($block, 'show-continue', false);
		$content_to_show = parent::get_setting($block, 'content-to-show', 'excerpt');
/** 
Show Excerpt
**/
		$show_item_excerpt_content = parent::get_setting($block, 'show-item-excerpt-content');
		$item_excerpt_content = parent::get_setting($block, 'item-excerpt-content');	
	/**
	Advanced Excerpt
	**/
		$my_excerpt_limit = parent::get_setting($block, 'my-excerpt-limit');
		$length_type = parent::get_setting($block, 'length-type'); // words, characters
		$no_custom = parent::get_setting($block, 'no-custom'); // 1, 0  (1 = excerpt  /  0 = custom excerpt will be used)
		$no_shortcode = parent::get_setting($block, 'no-shortcode'); // 1, 0  (1 = shortcode remove)  
		$finish = parent::get_setting($block, 'finish'); //exact, word, sentence
		$read_more = parent::get_setting($block, 'read-more'); //
		$add_link = parent::get_setting($block, 'add-link'); //
		$exclude_tags = parent::get_setting($block, 'exclude-tags'); //
		$allowed_tags = parent::get_setting($block, 'allowed-tags'); //
		$ellipsis = parent::get_setting($block, 'ellipsis'); //
	/**
	END Advanced Excerpt
	**/

	if (($show_item_title) and ($show_item_title_before)) {
		if ($title_length > $limit) 
			$title .= "...";
		if (!$shorten)
			$title = get_the_title($id);
		if(!$item_linked)
			$item_title = '<' . $html_tag . ' class="item-title">'. $title.'</' . $html_tag . '>';
			if($item_linked)
			$item_title = '<' . $html_tag . ' class="item-title">
			<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
			</' . $html_tag . '>';
			echo $item_title;
	}


if ( $show_item_excerpt_content == 'show-option') { 

?>
<div class="<?php echo $excerpt_content_class; ?>">
	<div class="item-excerpt">
	<?php
		if($item_excerpt_content ==	'excerpt-option'){
			the_excerpt(); 
		} elseif ($item_excerpt_content ==	'content-option'){
			the_content( );
		} elseif ($item_excerpt_content ==	'my-excerpt-option'){

			if(function_exists(the_advanced_excerpt)){

				$params = array( 
					'length' => $my_excerpt_limit,
					'length_type' => $length_type,
					'finish' => $finish,
					'no_custom' => $no_custom,
					'ellipsis'	=> $ellipsis,
					'exclude_tags'	=> $exclude_tags,
					'allowed_tags'	=> $allowed_tags,
					'read_more'	=> $read_more,
					'add_link'	=> $add_link,
					);

				the_advanced_excerpt($params); 
			}
		}
	?>
	<?php if (parent::get_setting($block, 'show-edit-link')) { 
		 edit_post_link();
		}
	?>
	</div><!-- excerpt -->
</div> 
<?php  } 

}

/**

Featured Image

**/
function featured_image($block, $featured_image_class){

	$show_images = parent::get_setting($block, 'show-images', true);
	$show_item_title = parent::get_setting($block, 'show-item-title', false);
	$show_item_title_before = parent::get_setting($block, 'show-item-title-before', true);

 if ($show_images) { ?>
	<div class="<?php echo $featured_image_class; ?>">
	<?php
	if ( has_post_thumbnail() ) {
		echo get_the_post_thumbnail( $post->ID, 'featured-thumb', array('title' => ''));
	}
	?>
		<div>
			<?php $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured' );	?>
		</div>
	</div>
	<?php }  // end if images 
}

/**

Anything in here will be displayed when the block is being displayed.

**/
	function content($block) {

	// echo	$post_type = parent::get_setting($block, 'post-type', 'post');
		$post_type = parent::get_setting($block, 'post-type', false);
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h3');
		
		$linked = parent::get_setting($block,'title-link', true);
		$title = parent::get_setting($block, 'title');

/**
* $approx_featured_width = (HeadwayBlocksData::get_block_width($block) / $columns);
* pozor hadze tuto chybu Warning: Division by zero in
**/

		/* Element Visibility */
		//$width = parent::get_setting($block, 'image-width', '260');
		$item_linked = parent::get_setting($block,'item-title-link', true);

		// if 	($direction  == 'up' || $direction == 'down'){
		// 		$responsive = 'false';
		// 		$width = '100%';
		// } else {
		// 		$responsive = 'true';
		// 		//$width = '240';
		// }

		/* Setup Query */
			$query_args = array();

			/* Categories */
				if ( parent::get_setting($block, 'categories-mode', 'include') == 'include' ) 
					$query_args['category__in'] = parent::get_setting($block, 'categories', array());

				if ( parent::get_setting($block, 'categories-mode', 'include') == 'exclude' ) 
					$query_args['category__not_in'] = parent::get_setting($block, 'categories', array());	

			/** Post Type **/	
				$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

			/* Posts Limit */
				$query_args['posts_per_page'] = parent::get_setting($block, 'shown-total', 10);

			/* Author Filter */
				if ( is_array(parent::get_setting($block, 'author')) )
					$query_args['author'] = trim(implode(',', parent::get_setting($block, 'author')), ', ');

			/* Order */
				$query_args['orderby'] = parent::get_setting($block, 'order-by', 'date');
				$query_args['order'] = parent::get_setting($block, 'order', 'DESC');

			/* Status */
				$query_args['post_status'] = 'publish';

				$query_args['post__in'] = parent::get_setting($block, 'post-id', array());

			/* Query! */
				$posts = new WP_Query($query_args);

				global $paged; /* Set paged to the proper number because WordPress pagination SUCKS!  ANGER! */
				$paged = $paged_var;
		/* End Query Setup */
?>
<?php 
/** 

Generating Slider

**/
?>
<!-- slider -->
<div class="flexslider loading">
	<div class="slides">
	<?php


	if(!empty($posts)):
		while ( $posts->have_posts() ) : $posts->the_post();
		?>
	<article class="row">
<?php

/** 

TEMP - dorobit HW options = default a nastavenie pre konkretny post bude cez ACF

**/

	if (function_exists('get_field') && get_field('slider_type') ) :
		
		switch (get_field('slider_type')){
			case not_set:
	 		// HW OPTION
	 		$slider_type = 'single_featured';
			break;
			
			// ACF OPTION
			default: 
			$slider_type = get_field('slider_type');

		} //end switch

	else :
	// IF not exist ACF
	 	$slider_type = 'single_featured';

	endif;



switch($slider_type) {

/**
Columns - Excerpt / Featured 
**/
	case columns_excerpt_featured:
		
		$excerpt_content_class = 'column column-1 grid-left-0 grid-width-12 item-image-block block';
		$featured_image_class = 'column column-2 grid-left-0 grid-width-12 item-image-block block';

		$this->excerpt_content($block, $excerpt_content_class);
		$this->featured_image($block, $featured_image_class);

		echo '<pre>Columns - Excerpt / Featured </pre>';
	
	break;
/**
Columns - Featured / Excerpt
**/
	case columns_featured_excerpt:

		$featured_image_class = 'column column-1 grid-left-0 grid-width-12 item-image-block block';
		$excerpt_content_class = 'column column-2 grid-left-0 grid-width-12 item-image-block block';

		$this->featured_image($block, $featured_image_class);
		$this->excerpt_content($block, $excerpt_content_class);

		echo '<pre>Columns - Featured / Excerpt</pre>';

	break;

/**
Single - Featured
**/

	case single_featured:
		
		$featured_image_class = 'item-image-block block';
		$excerpt_content_class = 'item-image-block block';
		
		$this->featured_image($block, $featured_image_class);
		
		echo '<pre>Single - Fatured</pre>';

	break;	

/**
Single - Excerpt
**/

	case single_excerpt:
		
		echo 'single excerpt';
		$featured_image_class = 'item-image-block block';
		$excerpt_content_class = 'item-image-block block';		
	
		$this->excerpt_content($block, $excerpt_content_class);

	break;
	
/**
Above - Excerpt / Featured
**/

	case above_excerpt_featured:

		echo 'above excerpt featured';
		
		$featured_image_class = 'item-image-block block';
		$excerpt_content_class = 'item-image-block block';
		
		$this->featured_image($block, $featured_image_class);

	break;

/**
Above - Featured / Excerpt
**/

	case above_featured_excerpt:

		$featured_image_class = 'item-image-block block';
		$excerpt_content_class = 'item-image-block block';
		
		$this->featured_image($block, $featured_image_class);
		$this->excerpt_content($block, $excerpt_content_class);
	
	break;

/** 
Default 
**/
	default: 
		
		$featured_image_class = 'column column-1 grid-left-0 grid-width-12 item-image-block block';
		$excerpt_content_class = 'column column-2 grid-left-0 grid-width-12 item-image-block block';

		$this->featured_image($block, $featured_image_class);
		$this->excerpt_content($block, $excerpt_content_class);

		echo '<pre>Columns - Featured / Excerpt</pre>';
}

?>
	</article> <!-- row -->
<?php 
endwhile;
endif;
if(!empty($posts))
?>
	</div> <!-- slides -->
</div><!-- flexslider-->
<div class="clear"></div>

<?php
/** 
	
	DEBUG 

**/		
	if (parent::get_setting($block, 'debug') == true) 	{
		
	echo '<div class="debug">
	 <pre>';
		print_r($query_args);
		echo 'width - '. $width .'<br>';

		echo 'easing - '. $easing_animation .'<br>';
		echo 'responsive - '.  $responsive.'<br>';
		echo 'fx - '.  $fx .'<br>';
		echo 'scroll speed - '. $scroll_speed .'<br>';
		echo 'direction - '. $direction .'<br>';
		echo 'shown_max - '. $shown_max .'<br>';
		echo 'shown_min - '. $shown_min .'<br>';

		echo 'Prev Nav - '. $prev_nav .'<br>';
		echo 'Next Nav - '. $next_nav .'<br>';
		echo 'Content - '. $content_to_show .'<br>';
		echo 'Show navigation - '. $show_navigation .'<br>';
		echo 'Show Item Title - '. $show_item_title .'<br>';
		echo 'Show Item Title Before - '. $show_item_title_before .'<br>';

		echo '</pre></div>';

	}

}

	/**
	 * Register elements to be edited by the Headway Design Editor
	 **/

	function setup_elements() {

		$this->register_block_element(array(
			'id' => 'flexslider',
			'name' => 'flexslider',
			'selector' => '.flexslider',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.flexslider:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'item-title',
			'name' => 'Title',
			'selector' => '.item-title',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.item-title a:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'item-title-link',
			'name' => 'Title Link',
			'selector' => '.item-title a',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.item-title a:hover', 
			)
		));
	
		$this->register_block_element(array(
			'id' => 'item-content',
			'name' => 'Content',
			'selector' => '.item-content',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			// 'states' => array(
			// 	'Hover' => '.flipper-item a:hover', 
			// )
		));

		$this->register_block_element(array(
			'id' => 'item-image-block',
			'name' => 'Image Block',
			'selector' => '.item-image-block',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.item-image:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'item-image-block-img',
			'name' => 'Image',
			'selector' => '.item-image-block img',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.item-image-block img:hover', 
			)
		));

		$this->register_block_element(array(
			'id' => 'item-excerpt',
			'name' => 'Excerpt',
			'selector' => '.item-excerpt',
		//	'properties' => array('fonts', 'background', 'borders', 'padding', 'rounded-corners', 'box-shadow', 'text-shadow'),
			'states' => array(
				'Hover' => '.item-excerpt:hover', 
			)
		));
	}

/****/
	public static function excerpt_more($more) {

		return '...';

	}

	public static function my_excerpt($limit) {
	  $excerpt = explode(' ', get_the_excerpt(), $limit);
	  if (count($excerpt)>=$limit) {
	    array_pop($excerpt);
	    $excerpt = implode(" ",$excerpt).'...';
	  } else {
	    $excerpt = implode(" ",$excerpt);
	  }	
	  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	  return $excerpt;
	}
	
}